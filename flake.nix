{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };
  outputs = { self, nixpkgs }:
    with nixpkgs.lib;
    let
      # jre8 supported platforms
      forAllSystems = genAttrs [
        "aarch64-darwin"
        "aarch64-linux"
        "i686-linux"
        "x86_64-darwin"
        "x86_64-linux"
      ];

      nixpkgsForSystem = forAllSystems (system:
        import nixpkgs {
          inherit system;
          config.allowUnfree = true;
        });
    in
    {
      packages = forAllSystems (system: {
        raritan-mpc = nixpkgsForSystem.${system}.callPackage ./. { };
      });

      defaultPackage = forAllSystems (system: self.packages.${system}.raritan-mpc);

      checks = forAllSystems (system:
        let
          pkgs = nixpkgsForSystem.${system};

          checks =
            mapAttrs'
              (name: value: {
                name = "packages-${name}";
                inherit value;
              })
              self.packages.${system};
        in
        checks // {
          all = pkgs.linkFarm "checks" (mapAttrsToList (name: path: { inherit name path; }) checks);
        });
    };
}
